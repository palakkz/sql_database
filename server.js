var express=require("express"),
    http=require("http"),
    mysql=require("mysql"),
    bodyParser=require("body-parser"),
    dateformat=require("dateformat"),
    // now=new Date(),
    methodOverride=require("method-override"),
    app=express();

app.use(methodOverride("_method"));  
app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());
app.use("/js",express.static(__dirname+"/node_module/bootstrap/dist/js"));
app.use("/js",express.static(__dirname+"/node_module/tether/dist/js"));
app.use("/js",express.static(__dirname+"/node_module/jquery/dist"));
app.use("/css",express.static(__dirname+"/node_module/bootstrap/dist/css"));
app.set("view engine","ejs");
const con=mysql.createConnection({
    host:"localhost",
    user:"newuser",
    password:"user_password",
    database:"db"
});
con.connect(function(err){
    if(err){
        throw err;
    }
    console.log("CONNECTED");
});
const baseURL="http://localhost:3000/";


app.get("/department",function(req,res){
    var sql="SELECT * FROM department";
        con.query(sql,function(err,result){
            if(err){
                throw err;
            }
            else{
                res.send(result);
            }
        });
        
});
app.get("/employee",function(req,res){
    
    con.query("SELECT * FROM employee",function(err,result){
        if(err){
            throw err;
        }
        else{
            
            res.send(result);
        }
    });
    
});
app.get("/salary",function(req,res){
    
    con.query("SELECT * FROM salary",function(err,result){
        if(err){
            throw err;
        }
        else{
           
            res.send(result);
        }
    });
    
});
app.get("/department/:id",function(req,res){
    con.query("SELECT * FROM department WHERE id="+req.params.id,function(err,result){
        if(err){
            throw err;

        }else{
            res.send(result);
        }
    });
});
app.get("/employee/:emp_id",function(req,res){
    con.query("SELECT * FROM employee WHERE emp_id="+req.params.emp_id,function(err,result){
        if(err){
            throw err;

        }else{
            res.send(result);
        }
    });
});
app.get("/salary/:id",function(req,res){
    con.query("SELECT * FROM salary WHERE id="+req.params.id,function(err,result){
        if(err){
            throw err;

        }else{
            res.send(result);
        }
    });
});



app.post("/department/add",function(req,res){
    

    var query="INSERT INTO department (name,created_date) VALUES (";
        query += "'"+req.body.name+"',";
        query += "'"+dateformat(req.body.created_date,"yyyy-mm-dd")+"')";
       
    con.query(query,function(err,result){
        if(err){
            throw err;
        }
        else{
            res.send(req.body);
            
        }
    
    });

});
app.post("/employee/add",function(req,res){
    var sq={
        name:req.body.name,
        emp_no:req.body.emp_no,
        join_date:req.body.join_date,
        end_date:req.body.end_date,
        dept_id:req.body.dept_id

    };
    
    var que="INSERT INTO employee SET ?";
    con.query(que,sq,function(err,result){
        if(err){
            throw err;
        }
        else{
            res.send(req.body);
        }
    });

});
app.post("/salary/add",function(req,res){
    var sql={
        emp_id:req.body.emp_id,
        month:req.body.month,
        year:req.body.year,
        amount:req.body.amount,
        generated_date:req.body.generated_date
    };
    var quer="INSERT INTO salary SET  ?";
   

    con.query(quer,sql,function(err,result){
        if(err){
            throw err;
        }else{
            res.send(req.body);
        }
    });

});



app.put("/department/:id",function(req,res){
  
    var query="UPDATE `department` SET `name`='"+req.body.name+"',`created_date`='"+req.body.created_date+"' WHERE `id`="+req.params.id+"";
        
    con.query(query,function(err,result){
        if(result.affectedRows>0){
           res.send(req.params.id);
            // res.redirect("/department");
        }else{
            res.sendStatus(400);
        }
    });
});
app.put("/employee/:emp_id",function(req,res){
  
    var query="UPDATE `employee` SET `name`='"+req.body.name+"',`emp_no`='"+req.body.emp_no+"',`join_date`='"+req.body.join_date+"',`end_date`='"+req.body.end_date+"',`dept_id`='"+req.body.dept_id+"' WHERE `emp_id`="+req.params.emp_id+"";
        
    con.query(query,function(err,result){
        if(result.affectedRows>0){
            res.send(req.params.emp_id);          
        }else{
            res.sendStatus(400);
        }
    });
});
app.put("/salary/:id",function(req,res){
  
    var query="UPDATE `salary` SET `emp_id`='"+req.body.emp_id+"',`month`='"+req.body.month+"',`year`='"+req.body.year+"',`amount`='"+req.body.amount+"',`generated_date`='"+req.body.generated_date+"' WHERE `id`="+req.params.id+"";
        
    con.query(query,function(err,result){
        if(result.affectedRows>0){
            res.send(req.params.id);            
        }else{
            res.sendStatus(400);
        }
    });
});
app.delete("/department/:id",function(req,res){
    con.query("DELETE FROM department WHERE id='"+req.params.id+"'",function(err,result){
        if(result.affectedRows>0){
            res.send(req.params.id);
           
        }else{
            res.sendStatus(400);
        }
    });
});



app.delete("/employee/:emp_id",function(req,res){
    con.query("DELETE FROM employee WHERE emp_id='"+req.params.emp_id+"'",function(err,result){
        if(result.affectedRows>0){
            res.send(req.params.emp_id);
        }else{
            res.sendStatus(400);
        }
    });
});




app.delete("/salary/:id",function(req,res){
    con.query("DELETE FROM salary WHERE id='"+req.params.id+"'",function(err,result){
        if(result.affectedRows>0){
            res.send(req.params.id);
        }else{
            res.sendStatus(400);
        }
    });
});

app.listen(3000,function(err){
    if(err){
        console.log(err);
    }else{
        console.log("SERVER CONNECTED!!");
    }
});