var mysql=require("mysql");
var conn=mysql.createConnection({
    host:'localhost',
    user:'user2',
    password:'password',
    database:'db'
});
// var sql="CREATE TABLE department (id INT AUTO_INCREMENT PRIMARY KEY,name VARCHAR(255),created_date DATE)";
// var sql="CREATE TABLE employee (emp_id INT AUTO_INCREMENT PRIMARY KEY,name VARCHAR(255),emp_no INT,join_date DATE,end_date DATE,dept_id INT references department(id)) ";
var sql="CREATE TABLE salary (id INT AUTO_INCREMENT PRIMARY KEY,emp_id INT references employee(emp_id),month VARCHAR(255),year INT,amount INT,generated_date DATETIME)"

conn.connect(function(err){
    if(err){
        throw err;
    }
    else{
        console.log("CONNECTED");
        conn.query(sql,function(err,result){
            if(err){
                throw err;
            }
            else{
                console.log("TABLE CREATED");
            }
        })
    }
});